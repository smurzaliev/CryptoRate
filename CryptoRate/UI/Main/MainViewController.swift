//
//  ViewController.swift
//  CryptoRate
//
//  Created by Samat Murzaliev on 12.04.2022.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    
    var mainViewModel = MainViewModel()
    
    private lazy var appLabel: UILabel = {
        let view = UILabel()
        view.text = "CryptoRate"
        view.font = .systemFont(ofSize: 30, weight: .medium)
        view.textColor = UIColor.init(named: "fontColor")
        view.textAlignment = .center
        
        return view
    }()
    
    private lazy var fiatPicker: UIPickerView = {
        let view = UIPickerView()
        view.delegate = self
        view.dataSource = self
        
        return view
    }()
    
    private lazy var cryptoView = UIView()
    
    private lazy var btcLogo: UIImageView = {
        let view = UIImageView(image: UIImage(named: "btc-logo"))
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var btcAmount: UILabel = {
        let view = UILabel()
        view.text = "Rate here"
        view.font = .systemFont(ofSize: 22, weight: .medium)
        view.textColor = UIColor.init(named: "smallTextColor")
        
        return view
    }()
    
    private lazy var fiatLabel: UILabel = {
        let view = UILabel()
        view.text = "USD"
        view.font = .systemFont(ofSize: 22, weight: .regular)
        view.textColor = UIColor.init(named: "smallTextColor")
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewModel.delegate = self
        setSubviews()
    }
    
    
    private func setSubviews() {
        view.backgroundColor = UIColor.init(named: "backColor")
        
        view.addSubview(appLabel)
        appLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeArea.top).offset(10)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        view.addSubview(fiatPicker)
        fiatPicker.snp.makeConstraints { make in
            make.bottom.equalTo(self.view.safeArea.bottom)
            make.left.right.equalToSuperview()
            make.height.equalToSuperview().dividedBy(3)
        }
        
        view.addSubview(cryptoView)
        cryptoView.snp.makeConstraints { make in
            make.height.equalTo(80)
            make.left.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(5)
            make.top.equalTo(appLabel.snp.bottom).offset(100)
        }
        
        cryptoView.addSubview(btcLogo)
        btcLogo.snp.makeConstraints { make in
            make.height.width.equalTo(80)
            make.left.top.equalToSuperview().offset(10)
        }
        
        cryptoView.addSubview(fiatLabel)
        fiatLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
        }
        
        cryptoView.addSubview(btcAmount)
        btcAmount.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
    }
    
}

extension MainViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return mainViewModel.currencyArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: mainViewModel.currencyArray[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(named: "smallTextColor") ?? UIColor.white])
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedFiat = mainViewModel.currencyArray[row]
        mainViewModel.getPrice(for: selectedFiat)
    }
    
}

extension MainViewController: MainViewModelDelegate {
    
    func didUpdatePrice(price: String, currency: String) {
        DispatchQueue.main.async {
            self.btcAmount.text = price
            self.fiatLabel.text = currency
        }
    }
    
    func didFailWithError(error: Error) {
        print("Failed with error: \(error)")
    }
}

