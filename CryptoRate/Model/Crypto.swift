//
//  Crypto.swift
//  CryptoRate
//
//  Created by Samat Murzaliev on 12.04.2022.
//

import Foundation

struct Crypto: Decodable {
    let rate: Double
}
